package simple_game.graphics;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import simple_game.graphics.sprites.MiscSprites;
import simple_game.graphics.sprites.MobSprites;
import simple_game.graphics.sprites.PlayerSprites;

public class Sprite {

	public static PlayerSprites playerSprite = new PlayerSprites();
	public static MobSprites mobSprites = new MobSprites();
	public static MiscSprites miscSprites = new MiscSprites();

	public Dimension size;
	public Color color;
	public List<BufferedImage> images = new ArrayList<BufferedImage>();

	public Sprite(int width, int height, Color color) {
		size = new Dimension(width, height);
		this.color = color;
	}

	public Sprite(BufferedImage image) {
		size = new Dimension(image.getWidth(), image.getHeight());
		images.add(image);
	}

	public Sprite(BufferedImage image, int width, int height) {
		size = new Dimension(width, height);
		images.add(image);
	}

	public Sprite(BufferedImage[] images) {
		size = new Dimension(images[0].getWidth(), images[0].getHeight());
		for (int i = 0; i < images.length; i++)
			this.images.add(images[i]);

	}

	public Sprite(BufferedImage[] images, int width, int height) {
		size = new Dimension(width, height);
		for (int i = 0; i < images.length; i++)
			this.images.add(images[i]);
	}

	public static BufferedImage loadImage(String path) {
		try {
			return ImageIO.read(Sprite.class.getResource(path));
		} catch (IOException e) {
			return null;
		}
	}

	public void update() {

	}

	public BufferedImage getImage() {
		return images.get(0);
	}

	public Dimension getSize() {
		return size;
	}

}
