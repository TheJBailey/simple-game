package simple_game.graphics;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;

import simple_game.main.utilities.Coordinate;

public class Screen {

	public int width, height;
	Graphics2D g2d;

	public static Coordinate offset = new Coordinate(0, 0);

	double skewX, skewY, transX, transY;
	boolean skewed = false, translated = false;

	public Screen(int width, int height, Graphics g) {
		this.width = width;
		this.height = height;
	}

	public void update(int width, int height, Graphics g) {
		this.width = width;
		this.height = height;
		this.g2d = (Graphics2D) g;
	}

	public Graphics getGraphics() {
		return g2d;
	}

	public void renderBackground(Graphics g) {

	}

	public void setOffset(double x, double y) {
		offset = new Coordinate(x, y);
	}

	public void skew(double x, double y) {
		g2d.shear(x, y);
	}

	public void translate(double x, double y) {
		g2d.translate(x, y);
	}

	public void skewNext(double x, double y) {
		skewX = x;
		skewY = y;
		skewed = true;
		skew(x, y);
	}

	public void translateNext(double x, double y) {
		transX = x;
		transY = y;
		translated = true;
		translate(x, y);
	}

	public void checkEffects() {
		if (skewed) {
			skew(-skewX, -skewY);
			skewed = false;
		}
		if (translated) {
			translate(-transX, -transY);
			translated = false;
		}

	}

	public void drawString(String string, int x, int y, Font font, boolean fixed) {
		if (fixed) {
			x -= offset.x;
			y -= offset.y;
		}
		g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);
		g2d.setFont(font);
		g2d.drawString(string, x, y);
		checkEffects();
	}

	public void drawRectangle(int x, int y, int width, int height, Color color, boolean fixed) {
		if (fixed) {
			x -= offset.x;
			y -= offset.y;
		}
		g2d.setPaint(color);
		g2d.drawRect(x, y, width, height);
		checkEffects();
	}

	public void drawImage(Image image, int x, int y, int w, int h, boolean fixed) {
		if (fixed) {
			x -= offset.x;
			y -= offset.y;
		}
		g2d.drawImage(image, x, y, w, h, null);
		checkEffects();
	}

	public void drawImage(Image image, int x, int y, boolean fixed) {
		if (fixed) {
			x -= offset.x;
			y -= offset.y;
		}
		g2d.drawImage(image, x, y, null);
		checkEffects();
	}

	public void fillRectangle(int x, int y, int width, int height, Color bgcolor, Color fgcolor, boolean fixed) {
		if (fixed) {
			x -= offset.x;
			y -= offset.y;
		}
		g2d.setPaint(bgcolor);
		g2d.drawRect(x, y, width, height);
		g2d.setPaint(fgcolor);
		g2d.fillRect(x, y, width, height);
		checkEffects();
	}

	public void fillRectangle(int x, int y, int width, int height, Color fg, Color bg, double angle, boolean fixed) {
		if (fixed) {
			x -= offset.x;
			y -= offset.y;
		}
		AffineTransform at = new AffineTransform();
		at.translate(x + width / 2, y + height / 2);
		at.rotate(angle);
		at.translate(-width / 2, -height / 2);
		g2d.setPaint(bg);
		g2d.drawRect(x, y, width, height);
		g2d.setPaint(fg);
		g2d.fillRect(x, y, width, height);
		checkEffects();
	}

	public void fillOval(int x, int y, int width, int height, Color color, boolean fixed) {
		if (fixed) {
			x -= offset.x;
			y -= offset.y;
		}
		g2d.setPaint(color);
		g2d.fillOval(x, y, width, height);
		checkEffects();
	}

	public void renderSprite(double x, double y, Sprite sprite, boolean fixed) {
		if (fixed) {
			x -= offset.x;
			y -= offset.y;
		}
		if (g2d != null) g2d.drawImage(sprite.getImage(), (int) x, (int) y, sprite.size.width, sprite.size.height, null);
		checkEffects();

	}

	public void renderSprite(double x, double y, double width, double height, Sprite sprite, double angle, boolean fixed) {
		if (fixed) {
			x -= offset.x;
			y -= offset.y;
		}
		if (g2d != null) {
			AffineTransform at = new AffineTransform();
			at.translate(x + width / 2, y + height / 2);
			at.rotate(angle);
			at.translate(-width / 2, -height / 2);
			g2d.drawImage(sprite.getImage(), at, null);
			checkEffects();
		}
	}

	public void renderProjectile(double x, double y, double width, double height, Sprite sprite, double angle) {
		x -= offset.x;
		y -= offset.y;
		if (g2d != null) {
			AffineTransform at = new AffineTransform();
			at.translate(x + width / 2, y + height / 2);
			at.rotate(angle);
			at.translate(-width / 2, -height / 2);
			g2d.drawImage(sprite.getImage(), at, null);
			checkEffects();
		}
	}

	public void renderMob(double x, double y, double width, double height, Sprite sprite, double angle) {
		x -= offset.x;
		y -= offset.y;
		if (g2d != null) {
			AffineTransform at = new AffineTransform();
			at.translate(x + width / 2, y + height / 2);
			at.rotate(angle);
			at.translate(-width / 2, -height / 2);
			g2d.drawImage(sprite.getImage(), at, null);
			checkEffects();
		}
	}

	public void setPaint(Color c) {
		g2d.setPaint(c);
	}
}
