package simple_game.graphics.sprites;

import simple_game.graphics.Sprite;

public class MiscSprites {

	public Sprite projectile_normal = new Sprite(Sprite.loadImage("/res/sprites/misc/projectiles/normal.png"));
	public Sprite projectile_normal_particle = new Sprite(Sprite.loadImage("/res/sprites/misc/projectiles/normal_particle.png"));
	public Sprite spawnSprite = new Sprite(Sprite.loadImage("/res/sprites/misc/spawn_sprite.png"));

	public Sprite pu_speed = new Sprite(Sprite.loadImage("/res/sprites/entities/powerups/speed.png"));
	public Sprite pu_missile = new Sprite(Sprite.loadImage("/res/sprites/entities/powerups/missile.png"));
	public Sprite pu_multishot = new Sprite(Sprite.loadImage("/res/sprites/entities/powerups/multishot.png"));
	public Sprite pu_health = new Sprite(Sprite.loadImage("/res/sprites/entities/powerups/health.png"));
	public Sprite pu_attackspeed = new Sprite(Sprite.loadImage("/res/sprites/entities/powerups/atkspeed.png"));
}
