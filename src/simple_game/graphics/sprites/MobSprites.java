package simple_game.graphics.sprites;

import simple_game.graphics.Sprite;

public class MobSprites {

	public Sprite l1_green = new Sprite(Sprite.loadImage("/res/sprites/mobs/level1/green/normal.png"));
	public Sprite l1_green_particle = new Sprite(Sprite.loadImage("/res/sprites/mobs/level1/green/particle.png"));

}
