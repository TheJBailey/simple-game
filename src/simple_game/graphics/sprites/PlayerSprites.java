package simple_game.graphics.sprites;

import simple_game.graphics.Sprite;

public class PlayerSprites {
	public Sprite normal = new Sprite(Sprite.loadImage("/res/sprites/player/35x35.png"));

	public Sprite orb = new Sprite(Sprite.loadImage("/res/sprites/entities/powerups/orb3.png"));

	public Sprite heartFull = new Sprite(Sprite.loadImage("/res/sprites/player/hud/heart_full.png"));
	public Sprite heartEmpty = new Sprite(Sprite.loadImage("/res/sprites/player/hud/heart_empty.png"));

}
