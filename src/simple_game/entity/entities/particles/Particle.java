package simple_game.entity.entities.particles;

import simple_game.entity.Entity;
import simple_game.graphics.Screen;
import simple_game.graphics.Sprite;
import simple_game.main.utilities.Coordinate;
import simple_game.main.utilities.Vector;

public class Particle extends Entity {

	public enum Shape {
		square, circle, polygon
	}

	private double friction = .5;
	private int duration = 20;

	private ParticleSpawner spawner;
	private Sprite sprite;

	private double speed = 1, mass = 1, accel = 0;
	Vector vec;

	public Particle(Coordinate coor, int size, int duration, double mass, double speed, Sprite sprite, ParticleSpawner spawner) {
		super((int) coor.x, (int) coor.y, size, size, null);
		vec = new Vector(random.nextGaussian(), random.nextGaussian());
		this.spawner = spawner;
		this.sprite = sprite;

		this.mass = mass;

		this.duration = random.nextInt(10) + duration;
		setSpeed(speed + random.nextDouble());

	}

	@Override
	public void update() {
		ticks++;
		double old_speed = speed;
		speed = applyFrictionTo(speed, friction);
		accel = speed - old_speed;

		angle += speed;
		bounds.applyVector(vec, speed);

		if (ticks > duration || getDistanceTraveled() > spawner.particleRadius) spawner.remove(this);

	}

	public double applyFrictionTo(double z, double friction) {
		if (z < 0) friction *= -1;
		z -= friction * mass * accel;
		return z;
	}

	@Override
	public void render(Screen screen) {
		screen.fillRectangle((int) bounds.x, (int) bounds.y, bounds.width, bounds.height, sprite.color, sprite.color, true);
	}

	public void setSpeed(double speed) {
		accel = speed;
		this.speed = speed;
		accel -= speed;
	}

	public Vector deflectAngle(double angle) {
		vec.nx = Vector.getX(angle) * -1;
		vec.ny = Vector.getY(angle) * -1;
		return vec;
	}

	public double getDistanceTraveled() {
		return spawner.getBounds().distanceFrom(this.getBounds());
	}

	@Override
	public void collisionAction(Entity e) {}

}