package simple_game.entity.entities.particles;

import java.util.ArrayList;
import java.util.List;

import simple_game.entity.Entity;
import simple_game.graphics.Screen;
import simple_game.graphics.Sprite;
import simple_game.level.Level;
import simple_game.main.utilities.Coordinate;

public class ParticleSpawner extends Entity {

	Entity entity;
	List<Particle> particles = new ArrayList<Particle>();
	Sprite sprite;

	public int particleRadius = Integer.MAX_VALUE;

	public ParticleSpawner(Coordinate coor, Sprite sprite, Entity entity, Level level) {
		super((int) coor.x, (int) coor.y, 0, 0, level);
		this.sprite = sprite;
		this.entity = entity;
	}

	public void setProperties(int numOfParticles, int size, int dur, double mass, double speed) {
		for (int i = 0; i < numOfParticles; i++) {
			Particle p = new Particle(bounds, size, dur, mass, speed, sprite, this);
			this.particles.add(p);
		}
	}

	@Override
	public void update() {
		for (int i = 0; i < particles.size(); i++)
			particles.get(i).update();

		if (particles.size() == 0) level.remove(this);
	}

	@Override
	public void render(Screen screen) {
		for (Particle particle : particles)
			particle.render(screen);
	}

	public void remove(Particle p) {
		particles.remove(p);
	}

	public void setSpeed(double speed) {
		for (Particle particle : particles)
			particle.setSpeed(speed);
	}

	public void enableParticleDeflection(double angle_var) {
		boolean bool;
		for (Particle particle : particles) {
			bool = random.nextBoolean();
			if (bool) particle.deflectAngle(entity.angle + random.nextDouble() * angle_var);
			else particle.deflectAngle(entity.angle - random.nextDouble() * angle_var);
		}
	}

	public boolean isSolid() {
		return false;
	}

	@Override
	public void collisionAction(Entity e) {
		// TODO Auto-generated method stub

	}
}
