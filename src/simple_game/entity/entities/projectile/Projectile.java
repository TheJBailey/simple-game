package simple_game.entity.entities.projectile;

import java.awt.Color;

import simple_game.SimpleGame;
import simple_game.entity.Entity;
import simple_game.entity.entities.particles.ParticleSpawner;
import simple_game.entity.mob.Mob;
import simple_game.entity.mob.player.Player;
import simple_game.graphics.Screen;
import simple_game.graphics.Sprite;
import simple_game.main.utilities.Coordinate;
import simple_game.main.utilities.Mesh;
import simple_game.main.utilities.Vector;

public abstract class Projectile extends Entity {
	public static int RATE_OF_FIRE = 60;

	public enum Type {
		normal, missle, multishot1, multishot2, multishot3, large, dps, aoe, slow
	}

	protected Mob mob;
	protected int xOrigin, yOrigin, dx, dy;

	public double range, damage, speed;

	protected int msTimer, trkrTimer;
	protected Sprite sprite;
	Vector vec;

	public Projectile(Coordinate origin, Sprite sprite, Mob mob, Vector vec) {
		super((int) origin.x, (int) origin.y, sprite.size.width, sprite.size.height, mob.getLevel());
		this.mob = mob;
		this.sprite = sprite;
		bounds.x = this.xOrigin = (int) origin.x;
		bounds.y = this.yOrigin = (int) origin.y;
		this.vec = vec;
		angle = vec.getAngle();
		Coordinate[] coords = { new Coordinate(origin.x + sprite.size.width, origin.y), new Coordinate(origin.x, origin.y),
								new Coordinate(origin.x, bounds.y + sprite.size.height),
								new Coordinate(origin.x + sprite.size.width, bounds.y + sprite.size.height) };
		mesh = new Mesh(this, coords);
	}

	public double getDistanceTraveled() {
		return Math.sqrt((Math.abs((xOrigin - bounds.x) * (xOrigin - bounds.x)) + (yOrigin - bounds.y) * (yOrigin - bounds.y)));
	}

	public void update() {
		bounds.applyVector(vec, speed);
		mesh.update(vec, speed, angle);

		if (getDistanceTraveled() > range) remove();
	}

	@Override
	public void kill() {
		ParticleSpawner ps = new ParticleSpawner(getCenterPoint(), new Sprite(2, 2, Color.white), this, level);
		ps.setProperties(5, 1, 10, 30, speed * .7);
		ps.particleRadius = 50;
		ps.enableParticleDeflection(.7);
		level.add(ps);
		remove();
	}

	@Override
	public void collisionAction(Entity e) {
		if (!(e instanceof Player) && !(e instanceof Projectile)) this.kill();
		if (e instanceof Mob) {
			mob.health -= damage;
		}

	}

	public void render(Screen screen) {
		screen.drawRectangle((int) bounds.x, (int) bounds.y, sprite.getSize().width, sprite.getSize().width, this.sprite.color, true);
		if (SimpleGame.RENDER_DEV_STATS) renderPoints(screen);
	}

	public Mob getMob() {
		return mob;
	}

	public boolean isSolid() {
		return true;
	}

}
