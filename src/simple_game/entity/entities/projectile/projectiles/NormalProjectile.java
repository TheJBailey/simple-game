package simple_game.entity.entities.projectile.projectiles;

import simple_game.entity.entities.projectile.Projectile;
import simple_game.entity.mob.Mob;
import simple_game.graphics.Sprite;
import simple_game.main.utilities.Coordinate;
import simple_game.main.utilities.Vector;

public class NormalProjectile extends Projectile {

	public static int RATE_OF_FIRE = 6;

	public NormalProjectile(Coordinate coor, Mob mob, Vector vec) {
		super(coor, new Sprite(3, 3, randomColor()), mob, vec);
		damage = 2;
		speed = 10;
		range = 200;
	}

}
