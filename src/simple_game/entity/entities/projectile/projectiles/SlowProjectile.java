package simple_game.entity.entities.projectile.projectiles;

import java.awt.Color;

import simple_game.SimpleGame;
import simple_game.entity.entities.projectile.Projectile;
import simple_game.entity.mob.Mob;
import simple_game.graphics.Screen;
import simple_game.graphics.Sprite;
import simple_game.main.utilities.Coordinate;
import simple_game.main.utilities.Vector;

public class SlowProjectile extends Projectile {

	public SlowProjectile(Coordinate coor, Mob mob, Vector vec) {
		super(coor, new Sprite(13, 13, Color.RED), mob, vec);
		damage = 5;
		speed = 2;
		range = 600;
	}

	public void render(Screen screen) {
		screen.fillOval((int) bounds.x, (int) bounds.y, sprite.getSize().width, sprite.getSize().width, this.sprite.color, true);
		if (SimpleGame.RENDER_DEV_STATS) renderPoints(screen);
	}
}
