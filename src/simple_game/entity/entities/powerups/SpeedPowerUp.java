package simple_game.entity.entities.powerups;

import simple_game.graphics.Sprite;
import simple_game.level.Level;
import simple_game.main.utilities.Coordinate;

public class SpeedPowerUp extends PowerUp {

	public SpeedPowerUp(Coordinate coor, Level level) {
		super(coor, Sprite.miscSprites.pu_speed, level);
	}

}
