package simple_game.entity.entities.powerups;

import simple_game.entity.Entity;
import simple_game.graphics.Screen;
import simple_game.graphics.Sprite;
import simple_game.level.Level;
import simple_game.main.utilities.Coordinate;
import simple_game.main.utilities.Vector;

public class PowerUp extends Entity {

	protected static double speed = 1;
	Sprite sprite;

	public PowerUp(Coordinate coor, Sprite sprite, Level level) {
		super((int) coor.x, (int) coor.y, sprite.getSize().width, sprite.getSize().height, level);
		this.sprite = sprite;
	}

	@Override
	public void update() {
		Vector vec = new Vector(0, 0);
		vec = bounds.getVectorRelativeTo(level.player.getCenterPoint());
		bounds.applyVector(vec, speed);
		angle += .03;
	}

	@Override
	public void render(Screen screen) {
		screen.renderSprite(bounds.x, bounds.y, sprite.getSize().width, sprite.getSize().height, sprite, angle, true);
	}

	@Override
	public void collisionAction(Entity e) {}
}
