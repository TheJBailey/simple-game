package simple_game.entity;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;

import simple_game.entity.mob.Mob;
import simple_game.graphics.Screen;
import simple_game.level.Level;
import simple_game.main.utilities.Coordinate;
import simple_game.main.utilities.Mesh;
import simple_game.main.utilities.Rectangle;

public abstract class Entity {

	public Level level;
	public Rectangle bounds;
	private boolean removed;

	protected int ticks;

	public double angle;
	public Mesh mesh;

	public static Random random = new Random();

	public Entity(int x, int y, int width, int height, Level level) {
		bounds = new Rectangle((double) x, (double) y, width, height);
		this.level = level;
	}

	public abstract void update();

	public abstract void render(Screen screen);

	public void testCollision(Entity e) {
		if (collision(e)) collisionAction(e);
	}

	public boolean mapCollision(double xa, double ya) {
		double xp = this.bounds.x + xa;
		double yp = this.bounds.y + ya;
		if (xp < level.getBound(Level.LEFT) || yp < level.getBound(Level.TOP) || xp > level.getBound(Level.RIGHT)
				|| yp > level.getBound(Level.DOWN)) {
			return true;
		}
		return false;
	}

	protected boolean collision(Entity e) {
		if (bounds.intersects(e.getBounds())) return true;
		return false;
	}

	// protected boolean collision(Entity e) {
	// for (Coordinate vert : mesh.getVerticies()) {
	// if (e.mesh.contains(vert)) return true;
	// }
	// return false;
	// }

	// protected boolean collision(Entity entity) {
	// if (!entity.isSolid()) return false;
	// int intersections = 0;
	// Coordinate[] verts = null, eVerts = null;
	// verts = mesh.getVerticies();
	// eVerts = entity.mesh.getVerticies();
	//
	// // Test the ray against all sides
	// for (int i = 0; i < verts.length; i++) {
	// Coordinate p = verts[i];
	// double xMin = Coordinate.getMin(eVerts, Coordinate.X);
	// double xMax = Coordinate.getMax(eVerts, Coordinate.X);
	// double yMin = Coordinate.getMin(eVerts, Coordinate.Y);
	// double yMax = Coordinate.getMax(eVerts, Coordinate.Y);
	// // checks bounding box for collision (skips iteration)
	// if (p.x < xMin || p.x > xMax || p.y < yMin || p.y > yMax) continue;
	// for (int i2 = 0; i2 < eVerts.length; i2++) {
	// // gets the next iteration of the array (handles indexArrayOutOfboundsException)
	// Coordinate coord1 = new Coordinate(xMin - 1, p.y);
	// Coordinate coord2 = new Coordinate(p.x, p.y);
	// int ni2 = i + 1 > eVerts.length - 1 ? 0 : i + 1;
	// // Test if current side intersects with ray.
	// if (areIntersecting(coord1, coord2, eVerts[i2], eVerts[ni2])) {
	// intersections++;
	// }
	// }
	// if ((intersections % 2) == 1) return true;
	// }
	// return false;
	//
	// }

	public boolean areIntersecting(Coordinate v1p1, Coordinate v1p2, Coordinate v2p1, Coordinate v2p2) {
		double d1, d2;
		double a1, a2, b1, b2, c1, c2;

		// Convert vector 1 to a line (line 1) of infinite length.
		// We want the line in linear equation standard form: A*x + B*y + C = 0
		a1 = v1p2.y - v1p1.y;
		b1 = v1p1.x - v1p2.x;
		c1 = (v1p2.x * v1p1.y) - (v1p1.x * v1p2.y);

		// Every point (x,y), that solves the equation above, is on the line,
		// every point that does not solve it, is either above or below the line.
		// We insert (x1,y1) and (x2,y2) of vector 2 into the equation above.
		d1 = (a1 * v2p1.x) + (b1 * v2p1.y) + c1;
		d2 = (a1 * v2p2.x) + (b1 * v2p2.y) + c1;

		// If d1 and d2 both have the same sign, they are both on the same side of
		// our line 1 and in that case no intersection is possible. Careful, 0 is
		// a special case, that's why we don't test ">=" and "<=", but "<" and ">".
		if (d1 > 0 && d2 > 0) return false;
		if (d1 < 0 && d2 < 0) return false;

		// We repeat everything above for vector 2.
		// We start by calculating line 2 in linear equation standard form.
		a2 = v2p2.y - v2p1.y;
		b2 = v2p1.x - v2p2.x;
		c2 = (v2p2.x * v2p1.y) - (v2p1.x * v2p2.y);

		// Calulate d1 and d2 again, this time using points of vector 1
		d1 = (a2 * v1p1.x) + (b2 * v1p1.y) + c2;
		d2 = (a2 * v1p2.x) + (b2 * v1p2.y) + c2;

		// Again, if both have the same sign (and neither one is 0),
		// no intersection is possible.
		if (d1 > 0 && d2 > 0) return false;
		if (d1 < 0 && d2 < 0) return false;

		// If we get here, only three possibilities are left. Either the two
		// vectors intersect in exactly one point or they are collinear
		// (they both lie both on the same infinite line), in which case they
		// may intersect in an infinite number of points or not at all.
		if ((a1 * b2) - (a2 * b1) == 0.0f) return false;

		// If they are not collinear, they must intersect in exactly one point.
		return true;
	}

	public abstract void collisionAction(Entity e);

	public ArrayList<Mob> mobsInRadius(int radius) {
		// TODO arrange from closest to furthest
		ArrayList<Mob> mobs = new ArrayList<Mob>();
		for (Entity entity : level.getEntities()) {
			if (!(entity instanceof Mob)) continue;
			Mob mob = (Mob) entity;
			double dist = this.getCenterPoint().distanceFrom(mob.getCenterPoint());
			if (dist <= radius) mobs.add(mob);
		}
		return mobs;
	}

	protected void kill() {
		remove();
	}

	protected void renderPoints(Screen screen) {
		screen.fillRectangle((int) mesh.getCenter().x - 5, (int) mesh.getCenter().y - 5, 10, 10, Color.MAGENTA, Color.black, angle, true);
		for (Coordinate point : mesh.getVerticies())
			screen.fillRectangle((int) point.x, (int) point.y, 2, 2, Color.pink, Color.black, angle, true);
		screen.drawRectangle((int) bounds.x, (int) bounds.y, bounds.width, bounds.height, Color.yellow, true);
	}

	public Coordinate getCenterPoint() {
		return new Coordinate(bounds.x + bounds.width / 2, bounds.y + bounds.height / 2);
	}

	public boolean isSolid() {
		return true;
	}

	public Rectangle getBounds() {
		return bounds;
	}

	public boolean isRemoved() {
		return removed;
	}

	public void remove() {
		this.removed = true;
		level.remove(this);
	}

	public Level getLevel() {
		return level;
	}

	public static Color randomColor() {
		switch (random.nextInt(9)) {
		case 0:
			return Color.yellow;
		case 1:
			return Color.blue;
		case 2:
			return Color.cyan;
		case 3:
			return Color.green;
		case 4:
			return Color.magenta;
		case 5:
			return Color.orange;
		case 6:
			return Color.pink;
		case 7:
			return Color.red;
		default:
			return Color.white;
		}
	}
}
