package simple_game.entity.mob;

import java.util.ArrayList;
import java.util.List;

import simple_game.entity.Entity;
import simple_game.entity.entities.powerups.PowerUp;
import simple_game.entity.entities.projectile.Projectile;
import simple_game.entity.entities.projectile.projectiles.NormalProjectile;
import simple_game.entity.entities.projectile.projectiles.SlowProjectile;
import simple_game.entity.mob.player.Player;
import simple_game.graphics.Sprite;
import simple_game.level.Level;
import simple_game.main.utilities.Coordinate;
import simple_game.main.utilities.Vector;

public abstract class Mob extends Entity {

	protected Sprite sprite;

	protected static List<Projectile> projectiles = new ArrayList<Projectile>();

	public double maxHealth, health, maxShield, shield, speed = 2;
	protected int maxLives, lives;
	protected int radius = 0;
	protected boolean tracked = false;

	public Mob(int x, int y, int width, int height, Level level) {
		super(x, y, width, height, level);
	}

	public Mob(int x, int y, Sprite sprite, Level level) {
		super(x, y, sprite.getSize().width, sprite.getSize().height, level);
		this.sprite = sprite;
		radius = bounds.width / 2;
	}

	public void move(Vector vec) {
		double xa = vec.nx * speed;
		double ya = vec.ny * speed;

		while (xa != 0) {
			if (Math.abs(xa) > 1) {
				if (!mapCollision(abs(xa), ya)) {
					this.bounds.x += abs(xa);
				}
				xa -= abs(xa);
			} else {
				if (!mapCollision(abs(xa), ya)) {
					this.bounds.x += xa;
				}
				xa = 0;
			}
		}

		while (ya != 0) {
			if (Math.abs(ya) > 1) {
				if (!mapCollision(xa, abs(ya))) {
					this.bounds.y += abs(ya);
				}
				ya -= abs(ya);
			} else {
				if (!mapCollision(xa, abs(ya))) {
					this.bounds.y += ya;
				}
				ya = 0;
			}
		}

		// fix this
		mesh.update(vec, speed, angle);
	}

	public int abs(double var) {
		if (var < 0) return -1;
		else return 1;
	}

	public void shoot(int fireRate, Projectile.Type type) {
		if (ticks < fireRate) return;
		else ticks = 0;
		double theta = Coordinate.getAngle(getCenterPoint(), getExitPoint());
		switch (type) {
		case aoe:
			break;
		case dps:
			break;
		case large:
			break;
		case missle:
			addProjectile(new NormalProjectile(getExitPoint(), this, Vector.getVectorFrom(theta)));
			break;
		case multishot1:
			addProjectile(new NormalProjectile(getExitPoint(), this, Vector.getVectorFrom(theta - .02)));
			addProjectile(new NormalProjectile(getExitPoint(), this, Vector.getVectorFrom(theta)));
			addProjectile(new NormalProjectile(getExitPoint(), this, Vector.getVectorFrom(theta + .02)));
			break;
		case multishot2:
			addProjectile(new NormalProjectile(getExitPoint(), this, Vector.getVectorFrom(theta - .025)));
			addProjectile(new NormalProjectile(getExitPoint(), this, Vector.getVectorFrom(theta - .0125)));
			addProjectile(new NormalProjectile(getExitPoint(), this, Vector.getVectorFrom(theta)));
			addProjectile(new NormalProjectile(getExitPoint(), this, Vector.getVectorFrom(theta + .0125)));
			addProjectile(new NormalProjectile(getExitPoint(), this, Vector.getVectorFrom(theta + .025)));
			break;
		case multishot3:
			addProjectile(new NormalProjectile(getExitPoint(), this, Vector.getVectorFrom(theta - .03)));
			addProjectile(new NormalProjectile(getExitPoint(), this, Vector.getVectorFrom(theta - .02)));
			addProjectile(new NormalProjectile(getExitPoint(), this, Vector.getVectorFrom(theta - .01)));
			addProjectile(new NormalProjectile(getExitPoint(), this, Vector.getVectorFrom(theta)));
			addProjectile(new NormalProjectile(getExitPoint(), this, Vector.getVectorFrom(theta + .01)));
			addProjectile(new NormalProjectile(getExitPoint(), this, Vector.getVectorFrom(theta + .02)));
			addProjectile(new NormalProjectile(getExitPoint(), this, Vector.getVectorFrom(theta + .03)));
			break;
		case normal:
			addProjectile(new NormalProjectile(getExitPoint(), this, Vector.getVectorFrom(theta)));
			break;
		case slow:
			addProjectile(new SlowProjectile(getExitPoint(), this, Vector.getVectorFrom(theta)));
			break;
		default:
			break;
		}
	}

	public void collisionAction(Entity e) {
		if (e instanceof Player) {
			this.kill();
			((Player) e).health -= this.health;
		}
		if (e instanceof PowerUp) {
			if (!(this instanceof Player)) return;
			level.player.givePowerUp((PowerUp) e);
			level.remove(e);
		}
		if (e instanceof Projectile) {
			if ((!(((Projectile) e).getMob() instanceof Player) && !(this instanceof Player))
					|| (this instanceof Player && (((Projectile) e).getMob() instanceof Player))) return;
			health -= ((Projectile) e).damage;
			((Projectile) e).kill();
		}
	}

	public void addProjectile(Projectile p) {
		projectiles.add(p);
		level.add(p);
	}

	public void removeProjectile(Projectile p) {
		projectiles.remove(p);
		level.remove(p);
	}

	public Coordinate getExitPoint() {
		double xp = this.bounds.x + bounds.width / 2;
		double yp = this.bounds.y + bounds.height / 2;
		xp += Math.cos(angle) * radius;
		yp += Math.sin(angle) * radius;
		return new Coordinate(xp, yp);
	}

	public double getAngleInRadians() {
		return angle;
	}

	public double getAngleInDegrees() {
		return Math.toDegrees(angle);
	}

	public boolean isBeingTracked() {
		return tracked;
	}

	public void setBeingTracked() {
		tracked = true;
	}

	public double getHealth() {
		return health;
	}

}
