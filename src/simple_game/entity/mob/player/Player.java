package simple_game.entity.mob.player;

import java.awt.Color;

import simple_game.SimpleGame;
import simple_game.entity.entities.powerups.AttackSpeedPowerUp;
import simple_game.entity.entities.powerups.HealthPowerUp;
import simple_game.entity.entities.powerups.MissilePowerUp;
import simple_game.entity.entities.powerups.MultiShotPowerUp;
import simple_game.entity.entities.powerups.PowerUp;
import simple_game.entity.entities.powerups.SpeedPowerUp;
import simple_game.entity.entities.projectile.Projectile;
import simple_game.entity.entities.projectile.Projectile.Type;
import simple_game.entity.mob.Mob;
import simple_game.graphics.Screen;
import simple_game.graphics.Sprite;
import simple_game.level.Level;
import simple_game.main.listeners.Keyboard;
import simple_game.main.listeners.Keyboard.Key;
import simple_game.main.listeners.Mouse;
import simple_game.main.utilities.Mesh;
import simple_game.main.utilities.Vector;

public class Player extends Mob {

	public enum Controller {
		MOUSE_KEYBOARD, KEYBOARD, JOYSTICK
	}

	Controller controller = Controller.MOUSE_KEYBOARD;

	Keyboard keys;

	public int score = 0, multiplier = 1;

	private int ccStep = 0, defFireRate = 6, fireRate = defFireRate, defSpeed = 2;
	private boolean godMode = false, regen = true;
	private double regenTimer = 5;

	public Projectile.Type currentType = Projectile.Type.normal;

	public Player(Keyboard keys, Level level) {
		super(0, 0, Sprite.playerSprite.normal, level);
		bounds.x = (level.getWidth() - bounds.width) / 2;
		bounds.y = (level.getHeight() - bounds.height) / 2;
		this.keys = keys;

		mesh = new Mesh(bounds, this, "/res/sprites/player/normal_mesh.dat");

		maxHealth = health = 100;
		radius = bounds.width / 2 - 6;
	}

	double tmpT;

	public void update() {
		if (!level.intro) {
			if (ticks > 10000) ticks = 0;
			ticks++;

			if (shield < maxShield) {
				regen = false;
				if (ticks - tmpT >= (60 * regenTimer)) regen = true;
			} else tmpT = ticks;

			if (regen) shield += 2;

			Vector vec = new Vector(0, 0);

			cheatCode(keys.contraCode);
			checkWeaponChange();

			switch (controller) {
			case JOYSTICK:
				break;
			case KEYBOARD:
				vec = keyMovement();
				if (keys.j.isActive()) angle += .1;
				if (keys.k.isActive()) angle -= .1;
				break;
			case MOUSE_KEYBOARD:
				vec = keyMovement();
				angle = this.getCenterPoint().getAngleRelativeTo(Mouse.getCoor());
				if (Mouse.getButton() == Mouse.Buttons.left) shoot(fireRate, currentType);
				break;
			default:
				break;
			}

			move(vec);

			if (godMode) health = 14;
			if (health <= 0) kill();
		}
	}

	@Override
	public void kill() {
		reset();
	}

	public Vector keyMovement() {
		Vector vec = new Vector(0, 0);
		if (keys.w.isActive()) vec.ny -= speed;
		if (keys.a.isActive()) vec.nx -= speed;
		if (keys.s.isActive()) vec.ny += speed;
		if (keys.d.isActive()) vec.nx += speed;
		if (keys.a.isActive() && keys.w.isActive()) vec = dualKeyMovement(-speed, -speed);
		if (keys.a.isActive() && keys.s.isActive()) vec = dualKeyMovement(-speed, speed);
		if (keys.d.isActive() && keys.w.isActive()) vec = dualKeyMovement(speed, -speed);
		if (keys.d.isActive() && keys.s.isActive()) vec = dualKeyMovement(speed, speed);
		return vec;
	}

	public Vector dualKeyMovement(double xa, double ya) {
		Vector vec = new Vector(0, 0);
		vec.nx = xa * Math.cos(45);
		vec.ny = ya * Math.sin(45);
		return vec;
	}

	public void checkWeaponChange() {}

	// public void playerCollision() {
	// for (int i = 0; i < level.getMobs().size(); i++) {
	// if (this.collision(level.getMobs().get(i))) {
	// this.takeDamage(1);
	// level.getMobs().get(i).kill();
	// }
	// }
	// }

	long timer = 0;

	public void cheatCode(Key[] code) {
		if (timer == 0) timer = System.currentTimeMillis();
		if (System.currentTimeMillis() - timer <= 500) {
			if (ccStep < code.length) {
				if (code[ccStep].isReleased()) {
					ccStep++;
					timer = System.currentTimeMillis();
					return;
				} else return;
			}
		} else {
			ccStep = 0;
			timer = 0;
		}

		if (ccStep == code.length) {
			ccStep = 0;
			timer = 0;
			if (godMode != true) activateGodMode();
			else deactivateGodMode();
		}
	}

	public void givePowerUp(PowerUp pu) {
		if (pu instanceof SpeedPowerUp) {
			if (speed < 2.75) speed += .25;
			return;
		}
		if (pu instanceof MultiShotPowerUp) {
			if (currentType == Projectile.Type.multishot2) currentType = Projectile.Type.multishot3;
			if (currentType == Projectile.Type.multishot1) currentType = Projectile.Type.multishot2;
			return;
		}
		if (pu instanceof MissilePowerUp) {
			currentType = Projectile.Type.missle;
			return;
		}
		if (pu instanceof AttackSpeedPowerUp) {
			if (fireRate > 3) fireRate--;
			return;
		}
		if (pu instanceof HealthPowerUp) {
			health += 20;
			if (health > maxHealth) health = maxHealth;
			return;
		}

	}

	public void activateGodMode() {
		System.out.println("god mode activated");
		godMode = true;
		speed = 5;
		health = 14;
	}

	public void deactivateGodMode() {
		System.out.println("god mode deactivated");
		godMode = false;
		speed = 2;
		health = 3;
	}

	@Override
	public void render(Screen screen) {
		screen.renderMob(bounds.x, bounds.y, bounds.width, bounds.height, sprite, angle);

		screen.skewNext(-.5, 0);
		screen.translateNext(25, 0);
		screen.fillRectangle(10, 10, (int) (200 * (health / maxHealth)), 30, Color.cyan, Color.cyan, false);

		screen.skewNext(-.5, 0);
		screen.translateNext(24, -1);
		screen.drawRectangle(10, 10, 201, 31, Color.white, false);

		if (SimpleGame.RENDER_DEV_STATS) renderPoints(screen);

		// screen.drawRectangle((int) bounds.x, (int) bounds.y, (int) bounds.width, (int)
		// bounds.height);
	}

	public int getScore() {
		return score * multiplier;
	}

	public void godMode(boolean bool) {
		godMode = bool;
	}

	public void reset() {
		level.clearEntities();
		health = maxHealth;
		speed = defSpeed;
		currentType = Type.normal;
		fireRate = defFireRate;
	}

}
