package simple_game.entity.mob.mobs;

import java.awt.Color;

import simple_game.SimpleGame;
import simple_game.entity.entities.particles.ParticleSpawner;
import simple_game.entity.entities.projectile.Projectile;
import simple_game.entity.mob.Mob;
import simple_game.graphics.Screen;
import simple_game.graphics.Sprite;
import simple_game.level.Level;
import simple_game.main.utilities.Coordinate;
import simple_game.main.utilities.Mesh;
import simple_game.main.utilities.Vector;

public class CircleMob extends Mob {

	public CircleMob(Coordinate coor, Level level) {
		super((int) coor.x, (int) coor.y, Sprite.mobSprites.l1_green, level);
		mesh = new Mesh(coor, this, "/res/sprites/mobs/level1/green/mesh.dat");
		maxHealth = health = 20;
		speed = .5;
	}

	@Override
	public void update() {
		ticks++;
		Vector vec = new Vector(0, 0);
		vec = getCenterPoint().getVectorRelativeTo(level.player.getBounds());
		if (random.nextInt(100) == 0) shoot(random.nextInt(40) + 120, Projectile.Type.slow);
		move(vec);
		mesh.update(vec, speed, angle);
		angle = bounds.getAngleRelativeTo(level.player.getBounds());

		if (health <= 0) kill();
	}

	public void kill() {
		ParticleSpawner ps = new ParticleSpawner(getCenterPoint(), new Sprite(3, 3, new Color(149, 41, 1)), this, level);
		ps.setProperties(30, 7, 40, 100, speed * 7);
		ps.particleRadius = 100;
		level.add(ps);
		level.remove(this);
		if (random.nextInt(10) > 2) level.add(level.getRandomPowerUp(this.getCenterPoint()));
	}

	@Override
	public void render(Screen screen) {
		screen.renderMob((int) bounds.x, (int) bounds.y, bounds.width, bounds.height, sprite, angle);
		if (SimpleGame.RENDER_DEV_STATS) renderPoints(screen);
	}

}
