package simple_game.main.menu;

import java.awt.Color;
import java.awt.image.BufferedImage;

import simple_game.graphics.Screen;
import simple_game.main.utilities.Rectangle;

public abstract class UIModule {

	protected Menu menu;
	protected Rectangle bounds;
	protected int padding = 0;

	protected BufferedImage image;

	protected Color background_color = Color.white, foreground_color = Color.cyan, border_color = Color.gray;
	protected Color text_color = Color.black, focus_color = Color.yellow;

	public UIModule(int x, int y, int width, int height) {
		bounds = new Rectangle(x, y, width, height);
	}

	public abstract void update();

	public void render(Screen screen) {
		screen.drawImage(image, getX(), getY(), false);
	}

	public Rectangle getBounds() {
		return bounds;
	}

	public int getX() {
		return (int) bounds.x + padding;
	}

	public int getY() {
		return (int) bounds.y + padding;
	}

	public int getWidth() {
		return bounds.width + (padding * 2);
	}

	public int getHeight() {
		return bounds.height + (padding * 2);
	}

	public int getPadding() {
		return padding;
	}

}
