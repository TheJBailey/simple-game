package simple_game.main.menu;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import simple_game.SimpleGame;
import simple_game.graphics.Screen;
import simple_game.main.menu.modules.Container;
import simple_game.main.utilities.FileUtil;

public abstract class Menu {

	public static Menu start = new PauseMenu("res/menu/start");
	public static Menu pause = new PauseMenu("res/menu/pause");
	public static Menu options = new PauseMenu("res/menu/options");

	protected Color background_color = new Color(.1f, .1f, .1f, .1f);
	protected BufferedImage bg_image;
	protected String directory;
	protected Document layout;

	protected List<UIModule> utilities = new ArrayList<UIModule>();

	public Menu() {

	}

	// TODO make sure it works exported
	// TODO plan on loading xml sheets for menu layout
	public Menu(String dir) {
		this.directory = dir;

		if (layout == null) try {
			load();
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("failed to load");
		}
		if (layout != null) init();
	}

	private void init() {
		Element element = layout.getDocumentElement();

		NodeList node_list = element.getChildNodes();
		parse(node_list);
	}

	private void parse(NodeList nl) {
		for (int i = 0; i < nl.getLength(); i++) {
			if (nl.item(i) instanceof Element) {
				Element ele = (Element) nl.item(i);
				initElement(ele);
			}
		}
	}

	private void initElement(Element ele) {
		switch (ele.getNodeName()) {
		case "container":
			this.add(new Container(Container.LAYOUT_CENTER, Container.FLOW_VERTICAL, ele, this));
			break;
		case "button":
			// this.add(new Button());
			break;
		default:
			break;
		}
	}

	public void load() throws IOException {
		try {
			final String path = directory;
			final File jarFile = new File(getClass().getProtectionDomain().getCodeSource().getLocation().getPath());
			if (jarFile.isFile()) { // Run with JAR file
				JarFile jar;
				jar = new JarFile(jarFile);
				final Enumeration<JarEntry> entries = jar.entries(); // gives ALL entries in jar
				while (entries.hasMoreElements()) {
					final String name = entries.nextElement().getName();
					if (name.startsWith(path + "/")) { // filter according to the path
						System.out.println(name);
					}
				}
				jar.close();
			} else { // Run with IDE
				final URL url = Menu.class.getResource("/" + path);
				if (url != null) {
					try {
						final File apps = new File(url.toURI());
						for (File file : apps.listFiles()) {
							if (file.getName().equals("layout.xml")) layout = FileUtil.parseXMLFile(file);
						}
					} catch (URISyntaxException ex) {
						System.err.println("never happens");
					}
				} else System.err.println("url is null");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void update() {
		updateComponent();
		for (UIModule utility : utilities)
			utility.update();
	}

	public abstract void updateComponent();

	public void render(Screen screen) {
		if (bg_image != null) screen.drawImage(bg_image, 0, 0, bg_image.getWidth(), bg_image.getHeight(), false);
		else screen.fillRectangle(0, 0, SimpleGame.getFrameWidth(), SimpleGame.getFrameHeight(), background_color, background_color, false);
		for (UIModule utility : utilities)
			utility.render(screen);

	}

	public void add(UIModule u) {
		utilities.add(u);
	}

	public void remove(UIModule u) {
		utilities.remove(u);
	}

	// TODO menu setup
	static class PauseMenu extends Menu {

		public PauseMenu(String dir) {
			super(dir);
		}

		@Override
		public void updateComponent() {

		}

	}

	static class StartMenu extends Menu {

		public StartMenu(String dir) {
			super(dir);
		}

		@Override
		public void updateComponent() {

		}

	}

	static class OptionsMenu extends Menu {

		public OptionsMenu(String dir) {
			super(dir);
		}

		@Override
		public void updateComponent() {

		}

	}
}
