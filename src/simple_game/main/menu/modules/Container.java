package simple_game.main.menu.modules;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import simple_game.SimpleGame;
import simple_game.graphics.Screen;
import simple_game.main.menu.Menu;
import simple_game.main.menu.UIModule;
import simple_game.main.utilities.FileUtil;

public class Container extends UIModule {

	public static int LAYOUT_CENTER = 0;
	public static int LAYOUT_LEFT = 1;
	public static int LAYOUT_RIGHT = 2;
	public static int LAYOUT_BOTTOM = 3;
	public static int LAYOUT_TOP = 4;

	public static int FLOW_VERTICAL = 0;
	public static int FLOW_HORIZONTAL = 1;
	public static int FLOW_GRID = 2;

	ArrayList<UIModule> children = new ArrayList<UIModule>();

	public Container(int layoutType, int flowType, Element e, Menu menu) {
		super(-1, -1, -1, -1);
		this.menu = menu;
		load(e);
		layout(layoutType, flowType);
	}

	public Container(int x, int y, int width, int height) {
		super(x, y, width, height);
	}

	public void load(Element e) {
		NodeList nodes = e.getChildNodes();

		for (int i = 0; i < nodes.getLength(); i++) {
			if (!(nodes.item(i) instanceof Element)) continue;
			String fileName = ((Element) nodes.item(i)).getAttribute("src");
			if (fileName.equals("")) return;
			File file = FileUtil.getResourceFile(fileName);
			if (nodes.item(i).getNodeName().equals("img")) {
				try {
					BufferedImage image = ImageIO.read(file);
					children.add(new UIImage(0, 0, image, 20));
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
			if (nodes.item(i).getNodeName().equals("button")) {
				try {
					BufferedImage image = ImageIO.read(file);
					children.add(new Button(0, 0, image, 10));
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
		/* OLD METHOD */
		/*
		 * { NodeList buttonNodes = e.getElementsByTagName("button"); String fileName; for (int i = 0; i < buttonNodes.getLength(); i++) {
		 * fileName = ((Element) buttonNodes.item(i)).getAttribute("src"); File file = FileUtil.getResourceFile(fileName); try { BufferedImage
		 * image = ImageIO.read(file); children.add(new Button(0, 0, image)); } catch (IOException e1) { e1.printStackTrace(); } } } { NodeList
		 * imgNodes = e.getElementsByTagName("img"); String fileName; for (int i = 0; i < imgNodes.getLength(); i++) { fileName = ((Element)
		 * imgNodes.item(i)).getAttribute("src"); File file = FileUtil.getResourceFile(fileName); try { BufferedImage image = ImageIO.read(file);
		 * children.add(new UIImage(0, 0, image, 10)); } catch (IOException e1) { e1.printStackTrace(); } } }
		 */
	}

	private void layout(int type, int flow) {
		switch (type) {
		case 0:
			for (int i = 0; i < children.size(); i++) {
				UIModule child = children.get(i);
				if (child.getWidth() > bounds.width) bounds.width = child.getWidth();
				bounds.height += child.getHeight();
			}
			break;
		case 1:
			break;
		case 2:
			break;
		case 3:
			break;
		}

		switch (flow) {
		case 0:
			bounds.x = (SimpleGame.getFrameWidth() - bounds.width) / 2;
			bounds.y = (SimpleGame.getFrameHeight() - bounds.height) / 2;
			int ht = (int) bounds.y;
			for (int i = 0; i < children.size(); i++) {
				children.get(i).getBounds().x = bounds.x + (bounds.width - children.get(i).getBounds().width) / 2;
				children.get(i).getBounds().y = ht;
				ht += children.get(i).getHeight();
			}
			break;
		case 1:
			break;
		case 2:
			break;
		case 3:
			break;
		}
	}

	@Override
	public void update() {
		for (UIModule child : children) {
			child.update();
		}
	}

	@Override
	public void render(Screen screen) {
		for (UIModule child : children) {
			child.render(screen);
		}
	}
}
