package simple_game.main.menu.modules;

import simple_game.SimpleGame;
import simple_game.main.menu.Menu;

public class Action {

	public enum Actions {
		SET_GAME_STATE_START("start"), SET_GAME_STATE_LOADING("loading"), SET_GAME_STATE_PLAYING("playing"), SET_GAME_STATE_PAUSED("paused"), SET_GAME_STATE_GAME_OVER(
				"gameover"),

		SET_GAME_DIFFICULTY_SCRUB("scrub"), SET_GAME_DIFFICULTY_AVERAGE_JOE("avgjoe"), SET_GAME_DIFFICULTY_PRO("pro"), SET_GAME_DIFFICULTY_GODLIKE(
				"god"), SET_GAME_DIFFICULTY_GLHF("glhf"),

		SET_GAME_MENU_START("start"), SET_GAME_MENU_OPTIONS("options"), SET_GAME_MENU_PAUSED("paused"), SET_GAME_MENU_SOUND("sound"), SET_GAME_MENU_GRAPHICS(
				"graphics"), SET_GAME_MENU_ADV_GRAPHICS("advgraphics"), SET_GAME_MENU_QUIT("quit"), SET_GAME_MENU_SAVE_LOAD("saveload");

		final String name;

		Actions(String string) {
			name = string;
		}
	}

	Actions action;

	public Action(Actions action) {
		this.action = action;
	}

	public void doAction() {
		switch (action) {
		case SET_GAME_DIFFICULTY_AVERAGE_JOE:
			break;
		case SET_GAME_DIFFICULTY_GLHF:
			break;
		case SET_GAME_DIFFICULTY_GODLIKE:
			break;
		case SET_GAME_DIFFICULTY_PRO:
			break;
		case SET_GAME_DIFFICULTY_SCRUB:
			break;
		case SET_GAME_MENU_ADV_GRAPHICS:
			break;
		case SET_GAME_MENU_GRAPHICS:
			break;
		case SET_GAME_MENU_OPTIONS:
			SimpleGame.menu = Menu.options;
			break;
		case SET_GAME_MENU_PAUSED:
			SimpleGame.menu = Menu.pause;
			break;
		case SET_GAME_MENU_QUIT:

			break;
		case SET_GAME_MENU_SAVE_LOAD:
			break;
		case SET_GAME_MENU_SOUND:
			break;
		case SET_GAME_MENU_START:
			SimpleGame.menu = Menu.start;
			break;
		case SET_GAME_STATE_GAME_OVER:
			break;
		case SET_GAME_STATE_LOADING:
			break;
		case SET_GAME_STATE_PAUSED:
			break;
		case SET_GAME_STATE_PLAYING:
			break;
		case SET_GAME_STATE_START:
			break;
		default:
			break;

		}
	}
}
