package simple_game.main.menu.modules;

import java.awt.image.BufferedImage;

import simple_game.graphics.Screen;
import simple_game.main.listeners.Mouse;
import simple_game.main.menu.UIModule;
import simple_game.main.utilities.Rectangle;

public class Button extends UIModule {

	public enum State {
		normal, hover, clicked;
	}

	public State state = State.normal;

	String text;

	BufferedImage image_hover;
	BufferedImage image_clicked;

	public Button(int x, int y, int width, int height) {
		super(x, y, width, height);
	}

	public Button(int x, int y, BufferedImage bgImage, int padding) {
		super(x, y, bgImage.getWidth(), bgImage.getHeight());
		this.image = bgImage;
		this.padding = padding;
	}

	@Override
	public void update() {
		if (Mouse.getBounds().intersects(new Rectangle(getX(), getY(), (int) bounds.width, (int) bounds.height))) {
			state = State.hover;
			if (Mouse.getButton() == Mouse.Buttons.left) state = State.clicked;
		} else state = State.normal;
	}

	@Override
	public void render(Screen screen) {
		screen.setPaint(background_color);
		if (state == State.normal) screen.drawImage(image, getX(), getY(), false);
		if (state == State.hover) screen.drawImage(image_hover, getX(), getY(), false);
		if (state == State.clicked) screen.drawImage(image_clicked, getX(), getY(), false);
	}

	public void setImages(BufferedImage image, BufferedImage hover, BufferedImage clicked) {
		bounds.setSize(image.getWidth(), image.getHeight());
		this.image = image;
		this.image_hover = hover;
		this.image_clicked = clicked;
	}

	public void setOffset(int xOff, int yOff) {
		bounds.x += xOff;
		bounds.y += yOff;
	}
}
