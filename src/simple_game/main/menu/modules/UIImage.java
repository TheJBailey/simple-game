package simple_game.main.menu.modules;

import java.awt.image.BufferedImage;

import simple_game.main.menu.UIModule;

public class UIImage extends UIModule {

	public UIImage(int x, int y, BufferedImage image, int padding) {
		super(x, y, image.getWidth(), image.getHeight());
		this.image = image;
		this.padding = padding;
	}

	@Override
	public void update() {}

}
