package simple_game.main.listeners;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import simple_game.graphics.Screen;
import simple_game.main.utilities.Coordinate;
import simple_game.main.utilities.Rectangle;

public class Mouse implements MouseListener, MouseMotionListener {

	public enum Buttons {
		none, left, right, middle, one, two, three, four, five, six, seven, eight, nine;

		int clicks;
	}

	private static Buttons button;
	private static Rectangle bounds;

	public Mouse() {
		bounds = new Rectangle();
		bounds.setSize(0, 0);
	}

	public static int getX() {
		return (int) (bounds.x + Screen.offset.x);
	}

	public static int getY() {
		return (int) (bounds.y + Screen.offset.y);
	}

	public static Rectangle getBounds() {
		return new Rectangle(getX(), getY(), 0, 0);
	}

	public static Coordinate getCoor() {
		return new Coordinate(getX(), getY());
	}

	public static Buttons getButton() {
		return button;
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		bounds.x = e.getX();
		bounds.y = e.getY();
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		bounds.x = e.getX();
		bounds.y = e.getY();
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		switch (e.getButton()) {
		case 1:
			Buttons.left.clicks++;
			break;
		case 2:
			Buttons.middle.clicks++;
			break;
		case 3:
			Buttons.right.clicks++;
			break;
		case 4:
			Buttons.one.clicks++;
			break;
		case 5:
			Buttons.two.clicks++;
			break;
		case 6:
			Buttons.three.clicks++;
			break;
		case 7:
			Buttons.four.clicks++;
			break;
		case 8:
			Buttons.five.clicks++;
			break;
		case 9:
			Buttons.six.clicks++;
			break;
		case 10:
			Buttons.seven.clicks++;
			break;
		case 11:
			Buttons.eight.clicks++;
			break;
		case 12:
			Buttons.nine.clicks++;
			break;
		default:
			break;
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {

	}

	@Override
	public void mouseExited(MouseEvent e) {

	}

	@Override
	public void mousePressed(MouseEvent e) {
		switch (e.getButton()) {
		case 1:
			button = Buttons.left;
			break;
		case 2:
			button = Buttons.middle;
			break;
		case 3:
			button = Buttons.right;
			break;
		case 4:
			button = Buttons.one;
			break;
		case 5:
			button = Buttons.two;
			break;
		case 6:
			button = Buttons.three;
			break;
		case 7:
			button = Buttons.four;
			break;
		case 8:
			button = Buttons.five;
			break;
		case 9:
			button = Buttons.six;
			break;
		case 10:
			button = Buttons.seven;
			break;
		case 11:
			button = Buttons.eight;
			break;
		case 12:
			button = Buttons.nine;
			break;
		default:
			button = Buttons.none;
			break;
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		button = Buttons.none;
	}

}
