package simple_game.main.listeners;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

public class Keyboard implements KeyListener {

	public Key ALT = new Key(KeyEvent.VK_ALT);
	public Key CTRL = new Key(KeyEvent.VK_CONTROL);
	public Key WIN_KEY = new Key(KeyEvent.VK_WINDOWS);
	public Key F3 = new Key(KeyEvent.VK_F3);
	public Key ESC = new Key(KeyEvent.VK_ESCAPE);
	public Key ENTER = new Key(KeyEvent.VK_ENTER);
	public Key SLASH = new Key(KeyEvent.VK_SLASH);

	public Key left = new Key(KeyEvent.VK_LEFT);
	public Key right = new Key(KeyEvent.VK_RIGHT);
	public Key up = new Key(KeyEvent.VK_UP);
	public Key down = new Key(KeyEvent.VK_DOWN);

	public Key w = new Key(KeyEvent.VK_W);
	public Key a = new Key(KeyEvent.VK_A);
	public Key s = new Key(KeyEvent.VK_S);
	public Key d = new Key(KeyEvent.VK_D);
	public Key b = new Key(KeyEvent.VK_B);
	public Key j = new Key(KeyEvent.VK_J);
	public Key k = new Key(KeyEvent.VK_K);
	public Key l = new Key(KeyEvent.VK_L);
	public Key i = new Key(KeyEvent.VK_I);
	public Key o = new Key(KeyEvent.VK_O);
	public Key u = new Key(KeyEvent.VK_U);
	public Key q = new Key(KeyEvent.VK_Q);
	public Key e = new Key(KeyEvent.VK_E);
	public Key r = new Key(KeyEvent.VK_R);
	public Key f = new Key(KeyEvent.VK_F);
	public Key z = new Key(KeyEvent.VK_Z);
	public Key x = new Key(KeyEvent.VK_X);
	public Key c = new Key(KeyEvent.VK_C);
	public Key p = new Key(KeyEvent.VK_P);

	public Key[] number = { new Key(KeyEvent.VK_0),
							new Key(KeyEvent.VK_1),
							new Key(KeyEvent.VK_2),
							new Key(KeyEvent.VK_3),
							new Key(KeyEvent.VK_4),
							new Key(KeyEvent.VK_5),
							new Key(KeyEvent.VK_6),
							new Key(KeyEvent.VK_7),
							new Key(KeyEvent.VK_8),
							new Key(KeyEvent.VK_9) };
	
	public Key[] contraCode = { up, up, down, down, left, right, left, right, b, a };

	boolean[] keyVals = new boolean[5000];
	static ArrayList<Key> keys = new ArrayList<Key>();

	public void update() {
		for (Key key : keys) {
			key.active = keyVals[key.value];// release / pressed logic
			if (key.prekey && !key.active) key.released = true;
			else key.released = false;
			if (!key.prekey && key.active) key.pressed = true;
			else key.pressed = false;
			key.prekey = key.active;
		}
	}

	public void addKey(Key key) {
		keys.add(key);
	}

	@Override
	public void keyPressed(KeyEvent e) {
		keyVals[e.getKeyCode()] = true;
	}

	@Override
	public void keyReleased(KeyEvent e) {
		keyVals[e.getKeyCode()] = false;

	}

	@Override
	public void keyTyped(KeyEvent e) {}

	public static class Key {
		boolean active, pressed, released, prekey;
		int value;

		public Key(int val) {
			this.value = val;
			keys.add(this);
		}

		public boolean isActive() {
			return active;
		}

		public boolean isReleased() {
			return released;
		}

		public boolean isPressed() {
			return pressed;
		}
	}
}

// import java.awt.event.KeyEvent;
// import java.awt.event.KeyListener;
// import java.util.ArrayList;
// import java.util.List;
//
// public class Keyboard implements KeyListener {
//
// private boolean[] key_codes = new boolean[1500];
// private List<Key> keys = new ArrayList<Key>();
//
// public void update() {
// for (Key key : keys) {
// key.update();
// if (key.isActive()) {
// key.setActive(key_codes[key.getKeyCode()]);
// if (!key.isActive()) key.release();
// } else key.setActive(key_codes[key.getKeyCode()]);
// }
// }
//
// @Override
// public void keyPressed(KeyEvent e) {
// key_codes[e.getKeyCode()] = true;
// }
//
// @Override
// public void keyReleased(KeyEvent e) {
// key_codes[e.getKeyCode()] = false;
// }
//
// @Override
// public void keyTyped(KeyEvent e) {}
//
// public void addKey(int e) {
// keys.add(new Key(e));
// }
//
// public void addKey(Key key) {
// keys.add(key);
// }
//
// public void addKey(Key... keys) {
// for (Key key : keys)
// this.keys.add(key);
// }
//
// public void addKeys(Key[] keys) {
// for (Key key : keys)
// this.keys.add(key);
// }
//
// static class Key {
//
// static Key w = new Key(KeyEvent.VK_W);
// static Key a = new Key(KeyEvent.VK_A);
// static Key s = new Key(KeyEvent.VK_S);
// static Key d = new Key(KeyEvent.VK_D);
//
// static Key b = new Key(KeyEvent.VK_B);
// static Key left = new Key(KeyEvent.VK_LEFT);
// static Key right = new Key(KeyEvent.VK_RIGHT);
// static Key down = new Key(KeyEvent.VK_DOWN);
// static Key up = new Key(KeyEvent.VK_UP);
//
// static Key space = new Key(KeyEvent.VK_SPACE);
// static Key shift = new Key(KeyEvent.VK_SHIFT);
//
// static Key[] number = { new Key(KeyEvent.VK_0), new Key(KeyEvent.VK_1), new Key(KeyEvent.VK_2),
// new Key(KeyEvent.VK_3), new Key(KeyEvent.VK_4) };
// static Key[] contraCode = { up, up, down, down, left, right, left, right, b, a };
//
// private int key_code;
// private boolean active, released;
//
// public Key(int key_code) {
// this.key_code = key_code;
// addKey(this);
// }
//
// public void update() {
// released = false;
// }
//
// public void setActive(boolean bool) {
// active = bool;
// }
//
// public boolean isActive() {
// return active;
// }
//
// public void release() {
// released = true;
// }
//
// public boolean isReleased() {
// return released;
// }
//
// public int getKeyCode() {
// return key_code;
// }
//
// public boolean isKey(int kc) {
// if (key_code == kc) return true;
// else return false;
// }
// }
//
// }
