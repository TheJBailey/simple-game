package simple_game.main.utilities;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.IOUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import simple_game.main.menu.Menu;

public abstract class FileUtil {

	private static File resultFile = null;

	public static String loadDataFile(String path) throws IOException {

		InputStream is = FileUtil.class.getResourceAsStream(path);
		String s = new String(IOUtils.toByteArray(is));
		is.close();

		return s;
	}

	public static ArrayList<File> getFilesInDirectory(File init_file, ArrayList<File> array) {
		return getFilesInDir(init_file, init_file, array);
	}

	private static ArrayList<File> getFilesInDir(File initFile, File rec_file, ArrayList<File> array) {
		if (rec_file.getName().equals(initFile.getName())) return addFilesInDir(initFile, rec_file, array);
		for (File file : rec_file.listFiles()) {
			if (file.isDirectory()) getFilesInDir(initFile, file, array);
		}
		return array;
	}

	private static ArrayList<File> addFilesInDir(File init_file, File rec_file, ArrayList<File> array) {
		for (File file : rec_file.listFiles()) {
			array.add(file);
		}
		return array;
	}

	public static ArrayList<File> getFilesInDirectory(String initFilePath, ArrayList<File> array) {
		final URL url = Menu.class.getResource(initFilePath);
		try {
			final File apps = new File(url.toURI());
			return getFilesInDirectory(apps, array);
		} catch (URISyntaxException e) {}
		return null;
	}

	public static File getResourceFile(String filename) {
		try {
			final File jarFile = new File(FileUtil.class.getProtectionDomain().getCodeSource().getLocation().getPath());
			if (jarFile.isFile()) { // Run with JAR file
				JarFile jar;
				jar = new JarFile(jarFile);
				final Enumeration<JarEntry> entries = jar.entries(); // gives ALL entries in jar
				while (entries.hasMoreElements()) {
					final String name = entries.nextElement().getName();
					if (name.startsWith("/res" + "/")) { // filter according to the path
						System.out.println(name);
					}
				}
				jar.close();
			} else { // Run with IDE
				final URL url = Menu.class.getResource("/res");
				try {
					final File apps = new File(url.toURI());
					resultFile = null;
					findFile(filename, apps);
					return resultFile;
				} catch (URISyntaxException ex) {
					System.out.println("url is the null");
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void findFile(String fileName, File root) {
		File[] files = root.listFiles();
		if (files == null) return;
		for (File file : files) {
			if (file.isDirectory()) findFile(fileName, file);
			else if (file.getName().equals(fileName)) resultFile = file;
		}
		return;
	}

	public static Document parseXMLFile(File file) {
		Document dom = null;
		// get the factory
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try {

			// Using factory get an instance of document builder
			DocumentBuilder db = dbf.newDocumentBuilder();

			// parse using builder to get DOM representation of the XML file
			dom = db.parse(file);

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (SAXException se) {
			se.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		return dom;
	}

	public static String getTextValue(Element ele, String tagName) {
		String textVal = null;
		NodeList nl = ele.getElementsByTagName(tagName);
		if (nl != null && nl.getLength() > 0) {
			Element el = (Element) nl.item(0);
			textVal = el.getFirstChild().getNodeValue();
		}

		return textVal;
	}

	public static int getIntValue(Element ele, String tagName) {
		// in production application you would catch the exception
		return Integer.parseInt(getTextValue(ele, tagName));
	}
}
