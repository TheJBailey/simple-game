package simple_game.main.utilities;

public class Vector {

	public double nx, ny;

	public Vector(double nx, double ny) {
		this.nx = nx;
		this.ny = ny;
	}

	public double getAngle() {
		return Math.atan2(ny, nx);
	}

	public double getMagnitude() {
		return Math.hypot(nx, ny);
	}

	public double getX() {
		return nx * this.getMagnitude();
	}

	public double getY() {
		return ny * this.getMagnitude();
	}

	public static Vector getVectorFrom(double angle) {
		return new Vector(Math.sin(angle), Math.cos(angle));
	}

	public static double getX(double angle) {
		return Math.cos(angle);
	}

	public static double getY(double angle) {
		return Math.sin(angle);
	}

}
