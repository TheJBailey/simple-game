package simple_game.main.utilities;

public class Coordinate {

	public static int X = 0;
	public static int Y = 1;

	public double x, y;

	public Coordinate() {
		x = y = -1;
	}

	public Coordinate(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public void applyVector(Vector vec, double speed) {
		x += vec.nx * speed;
		y += vec.ny * speed;
	}

	public Vector getVectorRelativeTo(Coordinate coor) {
		double dx = coor.x - x;
		double dy = coor.y - y;
		double angle = Math.atan2(dy, dx);
		return new Vector(Math.cos(angle), Math.sin(angle));
	}

	public double getAngleRelativeTo(Coordinate coor) {
		double dx = coor.x - x;
		double dy = coor.y - y;
		return Math.atan2(dy, dx);
	}

	public double distanceFrom(Coordinate coor) {
		double dx = (coor.x - this.x) * (coor.x - this.x);
		double dy = (coor.y - this.y) * (coor.y - this.y);
		return Math.sqrt(dx + dy);
	}

	public static Vector getVector(Coordinate coor_1, Coordinate coor_2) {
		double dx = coor_2.x - coor_1.x;
		double dy = coor_2.y - coor_1.y;
		double angle = Math.atan2(dy, dx);
		return new Vector(Math.cos(angle), Math.sin(angle));
	}

	public static double getMax(Coordinate[] coords, int var) {
		double max = Integer.MIN_VALUE;
		for (Coordinate coord : coords) {
			if (var == X) if (coord.x > max) max = coord.x;
			if (var == Y) if (coord.y > max) max = coord.y;
		}
		return max;
	}

	public static double getMin(Coordinate[] coords, int var) {
		double min = Integer.MAX_VALUE;
		for (Coordinate coord : coords) {
			if (var == X) if (coord.x < min) min = coord.x;
			if (var == Y) if (coord.y < min) min = coord.y;
		}
		return min;
	}

	public static double getAngle(Coordinate coor1, Coordinate coor2) {
		double dx = coor2.x - coor1.x;
		double dy = coor2.y - coor1.y;
		return Math.atan2(dx, dy);
	}

	public String toString() {
		return "(" + x + ", " + y + ")";
	}
}
