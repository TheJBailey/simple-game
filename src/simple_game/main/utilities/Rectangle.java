package simple_game.main.utilities;

public class Rectangle extends Coordinate {

	public int width , height;

	public Rectangle() {
		x = y = width = height = -1;
	}

	public Rectangle(double x, double y) {

		this.x = x;
		this.y = y;
		width = height = -1;
	}

	public Rectangle(int height, int width) {
		x = y = -1;
		this.width = width;
		this.height = height;
	}

	public Rectangle(double x, double y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	public void setSize(int width, int height) {
		this.width = width;
		this.height = height;
	}

	public Coordinate[] getVerticies() {
		Coordinate[] coords = new Coordinate[4];
		coords[0] = new Coordinate(x + width, y);
		coords[1] = new Coordinate(x, y);
		coords[2] = new Coordinate(x, y + height);
		coords[3] = new Coordinate(x + width, y + height);
		return coords;
	}

	public boolean intersects(Rectangle r) {
		double tw = this.width;
		double th = this.height;
		double rw = r.width;
		double rh = r.height;
		if (rw <= 0 || rh <= 0 || tw <= 0 || th <= 0) {
			return false;
		}
		double tx = this.x;
		double ty = this.y;
		double rx = r.x;
		double ry = r.y;
		rw += rx;
		rh += ry;
		tw += tx;
		th += ty;
		return ((rw < rx || rw > tx) && (rh < ry || rh > ty) && (tw < tx || tw > rx) && (th < ty || th > ry));
	}

}
