package simple_game.main.utilities;

import java.io.IOException;
import java.util.ArrayList;

import simple_game.entity.Entity;

public class Mesh {

	double angle;
	Coordinate[] points, originalPoints;
	Entity entity;

	public Mesh(Coordinate coor, Entity parent, String file) {
		try {
			load(FileUtil.loadDataFile(file));
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.entity = parent;
		for (int i = 0; i < this.points.length; i++) {
			this.points[i].x += coor.x;
			this.points[i].y += coor.y;
		}
	}

	public Mesh(Entity e, Coordinate... p) {
		this.originalPoints = this.points = p;
		this.entity = e;
	}

	public Mesh(Entity e, ArrayList<Coordinate> points) {
		this.originalPoints = this.points = new Coordinate[points.size()];
		for (int i = 0; i < points.size(); i++)
			this.originalPoints[i] = this.points[i] = points.get(i);
		this.entity = e;
	}

	private void load(String s) {
		ArrayList<Coordinate> points = new ArrayList<Coordinate>();
		String var = "";
		int num = 0;

		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			var = var.replaceAll("\n", "");
			var = var.replaceAll("\r", "");
			switch (c) {
			case ',':
				num = Integer.parseInt(var);
				var = "";
				break;
			case ';':
				points.add(new Coordinate(num, Integer.parseInt(var)));
				var = "";
				break;
			case ' ':
				break;
			default:
				var += c;
				break;
			}
		}
		this.originalPoints = this.points = new Coordinate[points.size()];
		for (int i = 0; i < points.size(); i++)
			this.originalPoints[i] = this.points[i] = points.get(i);
	}

	public void update(Vector vec, double speed, double angle) {
		points = getTranslatedPoints(vec, speed);
		points = getRotatedPoints(angle);
	}

	public Coordinate[] getRotatedPoints(double theta) {
		double delta = theta - angle;
		this.angle = theta;
		Coordinate[] coords = new Coordinate[points.length];
		for (int i = 0; i < coords.length; i++) {
			double xp, yp, rx, ry;
			// centers the shape on plane
			xp = points[i].x - this.getCenter().x;
			yp = points[i].y - this.getCenter().y;

			// rotates the point with a rotation matrix
			rx = (Math.cos(delta) * xp) + (-Math.sin(delta) * yp);
			ry = (Math.sin(delta) * xp) + (Math.cos(delta) * yp);

			// puts rotated points back in appropriate location
			rx += this.getCenter().x;
			ry += this.getCenter().y;

			coords[i] = new Coordinate(rx, ry);
		}
		return coords;
	}

	public Coordinate[] getTranslatedPoints(Vector vec, double speed) {
		Coordinate[] coords = new Coordinate[points.length];
		for (int i = 0; i < coords.length; i++) {
			coords[i] = new Coordinate(points[i].x + (vec.nx * speed), points[i].y + (vec.ny * speed));
		}
		return coords;
	}

	public Coordinate getCenter() {
		double cx = 0, cy = 0;
		for (Coordinate coor : points) {
			cx += coor.x;
			cy += coor.y;
		}
		return new Coordinate(cx / points.length, cy / points.length);
	}

	public boolean contains(Coordinate test) {
		int i;
		int j;
		boolean result = false;
		for (i = 0, j = points.length - 1; i < points.length; j = i++) {
			if ((points[i].y > test.y) != (points[j].y > test.y)
					&& (test.x < (points[j].x - points[i].x) * (test.y - points[i].y) / (points[j].y - points[i].y) + points[i].x)) {
				result = !result;
			}
		}
		return result;
	}

	public Coordinate[] getVerticies() {
		return this.points;
	}

	public Coordinate[] getOriginalPoints() {
		return this.originalPoints;
	}

	public Coordinate getMaxXPoint() {
		Coordinate maxCoord = new Coordinate(Integer.MAX_VALUE, 0);
		for (Coordinate coord : points)
			if (coord.x > maxCoord.x) maxCoord = coord;
		return maxCoord;
	}

	public Coordinate getMaxYPoint() {
		Coordinate maxCoord = new Coordinate(0, Integer.MIN_VALUE);
		for (Coordinate coord : points)
			if (coord.y > maxCoord.y) maxCoord = coord;
		return maxCoord;
	}

	public Coordinate getMinXPoint() {
		Coordinate minCoord = new Coordinate(Integer.MAX_VALUE, 0);
		for (Coordinate coord : points)
			if (coord.x < minCoord.x) minCoord = coord;
		return minCoord;
	}

	public Coordinate getMinYPoint() {
		Coordinate minCoord = new Coordinate(0, Integer.MAX_VALUE);
		for (Coordinate coord : points)
			if (coord.y < minCoord.y) minCoord = coord;
		return minCoord;
	}
}
