package simple_game.level;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import javax.imageio.ImageIO;

import simple_game.SimpleGame;
import simple_game.entity.Entity;
import simple_game.entity.entities.particles.ParticleSpawner;
import simple_game.entity.entities.powerups.AttackSpeedPowerUp;
import simple_game.entity.entities.powerups.HealthPowerUp;
import simple_game.entity.entities.powerups.MissilePowerUp;
import simple_game.entity.entities.powerups.MultiShotPowerUp;
import simple_game.entity.entities.powerups.PowerUp;
import simple_game.entity.entities.powerups.SpeedPowerUp;
import simple_game.entity.mob.mobs.CircleMob;
import simple_game.entity.mob.player.Player;
import simple_game.graphics.Screen;
import simple_game.main.utilities.Coordinate;
import simple_game.main.utilities.FileUtil;

public class Level {

	public static int LEFT = 0;
	public static int TOP = 1;
	public static int RIGHT = 2;
	public static int DOWN = 3;

	public static Level level5 = new Level("/res/level/level1/", "Level 5", null);
	public static Level level4 = new Level("/res/level/level1/", "Level 4", level5);
	public static Level level3 = new Level("/res/level/level1/", "Level 3", level4);
	public static Level level2 = new Level("/res/level/level1/", "Level 2", level3);
	public static Level level1 = new Level("/res/level/level1/", "Level 1", level2);
	public String name;
	// private static Sprite spawnSprite = Sprite.miscSprites.spawnSprite;

	int width, height;
	BufferedImage background, entityMap;

	ArrayList<Coordinate> spawn_points = new ArrayList<Coordinate>();
	ArrayList<Entity> entities = new ArrayList<Entity>();

	public Player player;

	public boolean intro = false, complete = false;
	private float stringOpacity = 1;

	public static Random random = new Random();
	public int ticks = 0;
	public double time;
	public int difficulty = 1;
	public Level nextLevel;

	public Level(int width, int height) {
		this.width = width;
		this.height = height;
		generateLevel();
	}

	public Level(String image, String name, Level nl) {
		nextLevel = nl;
		this.name = name;
		ArrayList<File> files = new ArrayList<File>();
		files = FileUtil.getFilesInDirectory(image, files);
		for (File file : files) {
			if (file.getName().equals("background.png")) {
				try {
					background = ImageIO.read(file);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (file.getName().equals("entityMap.png")) {
				try {
					entityMap = ImageIO.read(file);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		if (background == null) {
			try {
				background = ImageIO.read(FileUtil.getResourceFile("default_bg.png"));
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}

		width = background.getWidth();
		height = background.getHeight();

		generateLevel();
	}

	public void initPlayer(Player player) {
		this.player = player;
		add(player);
	}

	public void generateLevel() {
		int sw = background.getWidth() / entityMap.getWidth();
		int sh = background.getHeight() / entityMap.getHeight();
		int pw = background.getWidth() / sw;
		int ph = background.getHeight() / sh;

		int[] pixels = new int[pw * ph];
		entityMap.getRGB(0, 0, entityMap.getWidth(), entityMap.getHeight(), pixels, 0, entityMap.getWidth());

		for (int y = 0; y < ph; y++) {
			for (int x = 0; x < pw; x++) {
				if (pixels[x + y * entityMap.getWidth()] == 0xFF0099FF) entities.add(new CircleMob(new Coordinate(x * sw, y * sh), this));
			}
		}
		intro = true;
	}

	int tickInc = 1;

	private void time() {
		if (ticks >= 60) ticks = 0;
		ticks++;
		if (ticks % tickInc == 0) time += (tickInc / SimpleGame.ups);
		if (time > 3) intro = false;
		if (time > 1.5 && intro) stringOpacity -= 1.5 / (1.5 * SimpleGame.ups);
		if (stringOpacity < 0) stringOpacity = 0;
	}

	int tempTicks, spawnNum, activeSpawns;
	boolean waiting;

	public void update() {
		time();

		if (entities.size() == 1) complete = true;
		/*
		 * if (difficulty == 1) { tickInc = 60; activeSpawns = 4; } if (difficulty == 2) { activeSpawns = 6; tickInc = 30; } if (difficulty == 3)
		 * { tickInc = 30; activeSpawns = 8; } time(); if (ticks % tickInc == 0) { waiting = true; tempTicks = tickInc / 4 * 3; spawnNum =
		 * random.nextInt(activeSpawns); } if (waiting) { tempTicks--; if (tempTicks <= 0) { // entities.add(new
		 * CircleMob(spawn_points.get(spawnNum), this)); waiting = false; tempTi cks = 0; } }
		 */

		// if (time > 5.5) loading = false;
		if (!intro) for (int i = 0; i < entities.size(); i++)
			if (!(entities.get(i) instanceof Player)) entities.get(i).update();

		if (!intro) for (int i = 1; i <= entities.size(); i++) {
			Entity e = getEntities().get(i - 1);
			for (int i2 = i; i2 < entities.size(); i2++) {
				Entity e2 = getEntities().get(i2);
				if (!(e instanceof ParticleSpawner) && !(e2 instanceof ParticleSpawner)) e.testCollision(e2);
			}
		}

	}

	public void render(double xScroll, double yScroll, Screen screen) {
		screen.setOffset(xScroll, yScroll);
		if (background != null) screen.drawImage(background, (int) -xScroll, (int) -yScroll, false);
		/*
		 * if (waiting) screen.renderSprite(spawn_points.get(spawnNum).x - spawnSprite.getSize().width / 2, spawn_points.get(spawnNum).y -
		 * spawnSprite.getSize().height / 2, spawnSprite, false);
		 */

		for (Entity entity : entities)
			entity.render(screen);
	}

	public void renderHUD(Screen screen) {
		FontMetrics fm = screen.getGraphics().getFontMetrics();
		int sw = fm.stringWidth(name);
		int sh = fm.getHeight();
		if (intro) {
			screen.setPaint(new Color(0f, .5f, 1f, stringOpacity));
			screen.drawString(name, (screen.width - sw) / 2, (screen.height - sh) / 2, SimpleGame.DEFAULT_FONT, false);
		}
	}

	public PowerUp getRandomPowerUp(Coordinate coor) {
		switch (random.nextInt(5)) {
		case 0:
			return new SpeedPowerUp(coor, this);
		case 1:
			return new MultiShotPowerUp(coor, this);
		case 2:
			return new MissilePowerUp(coor, this);
		case 3:
			return new AttackSpeedPowerUp(coor, this);
		case 4:
			return new HealthPowerUp(coor, this);
		default:
			return new SpeedPowerUp(coor, this);
		}
	}

	public Level getNextLevel() {
		return nextLevel;
	}

	public String getLevelName() {
		return name;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public void add(Entity e) {
		entities.add(e);
	}

	public void remove(Entity e) {
		entities.remove(e);
	}

	public ArrayList<Entity> getEntities() {
		return entities;
	}

	public void clearEntities() {
		entities.clear();
	}

	public int getBound(int bound) {
		switch (bound) {
		case 0:
			return 100;
		case 1:
			return 100;
		case 2:
			return width - 170;
		case 3:
			return height - 137;
		default:
			return 0;
		}
	}

}
