package simple_game;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JTextField;

import simple_game.entity.mob.player.Player;
import simple_game.graphics.Screen;
import simple_game.level.Level;
import simple_game.main.listeners.Keyboard;
import simple_game.main.listeners.Mouse;
import simple_game.main.menu.Menu;
import simple_game.main.utilities.Coordinate;

public class SimpleGame extends Canvas implements Runnable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public static boolean RENDER_DEV_STATS = false;

	public static Font DEFAULT_FONT = new Font("Courier New", Font.PLAIN, 20);

	public enum State {
		playing, start, paused, options, loading, game_over;

		State previous;

		public void setPreviousState(State st) {
			previous = st;
		}
	}

	public static State state = State.playing;
	static JFrame frame;
	static int width = 1000, height = width / 16 * 9;
	public static Dimension size;

	Thread thread;
	boolean running;
	int ticks;

	public static double ups = 60.0;
	public static boolean GAME_OVER = false;

	static Keyboard keys;
	static Mouse mouse;

	static Level level;
	public static Player player;
	Screen screen;

	JTextField tf;

	public static Menu menu;

	BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
	Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(cursorImg, new Point(0, 0), "blank cursor");
	Cursor crosshair = Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR);

	public SimpleGame() {
		// Font[] fonts = GraphicsEnvironment.getLocalGraphicsEnvironment().getAllFonts();
		// for (int i = 0; i < fonts.length; ++i) {
		// System.out.print(fonts[i].getFontName() + " : ");
		// System.out.print(fonts[i].getFamily() + " : ");
		// System.out.println(fonts[i].getName());
		// }
		size = new Dimension(width, height);
		frame = new JFrame();
		setPreferredSize(size);
		setSize(getPreferredSize());

		screen = new Screen(size.width, size.height, null);

		keys = new Keyboard();
		addKeyListener(keys);

		mouse = new Mouse();
		addMouseListener(mouse);
		addMouseMotionListener(mouse);

		load(); // TODO fix menu system
	}

	public static void load() {
		// frame.setCursor(blankCursor);
		level = Level.level1;
		player = new Player(keys, level);
		level.initPlayer(player);
		setState(State.playing);
	}

	public void update() {
		keys.update();
		if (keys.ESC.isReleased()) RENDER_DEV_STATS = !RENDER_DEV_STATS;

		switch (state) {
		case playing:
			if (keys.p.isReleased()) setState(State.paused);
			player.update();
			level.update();
			break;
		default:
			menu.update();
			break;
		}
	}

	public void render() {
		BufferStrategy bs = getBufferStrategy();
		if (bs == null) {
			createBufferStrategy(2);
			return;
		}
		Graphics g = bs.getDrawGraphics();
		Graphics2D g2d = (Graphics2D) g;
		g2d.setPaint(new Color(.1f, .1f, .1f));
		g2d.fillRect(0, 0, width, height);
		initGraphics(g2d);
		screen.update(width, height, g2d);

		double xScroll = player.bounds.x - (screen.width - player.bounds.width / 2) / 2;
		double yScroll = player.bounds.y - (screen.height - player.bounds.height / 2) / 2;
		if (xScroll < 0) xScroll = 0;
		if (yScroll < 0) yScroll = 0;
		if (xScroll > level.getWidth() - screen.width) xScroll = level.getWidth() - screen.width;
		if (yScroll > level.getHeight() - screen.height) yScroll = level.getHeight() - screen.height;
		switch (state) {
		case paused:
			level.render(xScroll, yScroll, screen);
			player.render(screen);
			menu.render(screen);
			break;
		case playing:
			level.render(xScroll, yScroll, screen);
			player.render(screen);
			level.renderHUD(screen);
			if (level.complete) {
				if (level.getNextLevel() != null) {
					level = level.getNextLevel();
					level.initPlayer(player);
					player.level = level;
					player.bounds.x = (level.getWidth() - player.bounds.width) / 2;
					player.bounds.y = (level.getHeight() - player.bounds.height) / 2;
				}
			}
			break;
		case game_over:
			level.render(xScroll, yScroll, screen);
			player.render(screen);
			menu.render(screen);
			break;
		default:
			menu.render(screen);
			break;
		}

		if (RENDER_DEV_STATS) {
			screen.setPaint(Color.white);
			screen.drawString("m.x: " + Mouse.getCoor().x + ", m.y: " + Mouse.getCoor().y, 12, 20, DEFAULT_FONT, false);
		}

		g.dispose();
		bs.show();
	}

	public void initGraphics(Graphics2D g) {
		g.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
		g.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
		g.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
		g.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
		g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		g.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_NORMALIZE);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
	}

	public static void main(String[] args) {
		SimpleGame main = new SimpleGame();
		main.initWindow();
		main.start();
	}

	public void initWindow() {
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.add(this);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	public synchronized void start() {
		running = true;
		thread = new Thread(this, "Display");
		thread.start();
	}

	public synchronized void stop() {

	}

	public void run() {
		long lastTime = System.nanoTime();
		long timer = System.currentTimeMillis();
		final double ns = 1000000000.0 / ups;
		double delta = 0;
		int frames = 0, updates = 0;

		requestFocus();
		while (running) {
			long now = System.nanoTime();
			delta += (now - lastTime) / ns;
			lastTime = now;

			if (delta >= 1) {
				update();
				updates++;
				delta--;
			}
			render();
			frames++;

			if (System.currentTimeMillis() - timer > 1000) {
				timer += 1000;
				frame.setTitle("fps: " + frames + ", ups: " + updates);
				frames = 0;
				updates = 0;
			}
		}
	}

	public void reset() {
		level.clearEntities();
	}

	public static Level getLevel() {
		return level;
	}

	public static void setState(State st) {
		state.previous = state;
		state = st;
		switch (state) {
		case game_over:
			break;
		case loading:
			break;
		case options:
			break;
		case paused:
			menu = Menu.pause;
			break;
		case playing:
			break;
		case start:
			break;
		default:
			break;

		}
	}

	public void initCmd() {
		tf = new JTextField();
		tf.setBounds(0, height - 25, width, 25);
		tf.setBorder(javax.swing.BorderFactory.createEmptyBorder());
		tf.setBackground(Color.darkGray);
		tf.setForeground(Color.white);
		tf.setCaretColor(Color.white);
		tf.setVisible(false);
		frame.add(tf);

		tf.addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent e) {}

			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) hideCmd();
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					System.out.println("pressed");
					command(tf.getText());
					hideCmd();
				}
			}

			@Override
			public void keyTyped(KeyEvent e) {}
		});
	}

	public void showCmd() {
		tf.setVisible(true);
		tf.requestFocus();
	}

	public void hideCmd() {
		tf.setText("");
		tf.setVisible(false);
		requestFocus();
	}

	public void command(String cmd) {
		switch (cmd) {
		case "/kill":
			break;
		case "/godmode enable":
			player.godMode(true);
			break;
		case "/godmode disable":
			player.godMode(false);
			break;
		}
		tf.setText("");
	}

	public void setCursor(String img) throws IOException {
		BufferedImage cursorImage;

		cursorImage = ImageIO.read(getClass().getResource(img));
		for (int i = 0; i < cursorImage.getHeight(); i++) {
			int[] rgb = cursorImage.getRGB(0, i, cursorImage.getWidth(), 1, null, 0, cursorImage.getWidth() * 4);
			for (int j = 0; j < rgb.length; j++) {
				int alpha = (rgb[j] >> 24) & 255;
				if (alpha < 128) {
					alpha = 0;
				} else {
					alpha = 255;
				}
				rgb[j] &= 0x00ffffff;
				rgb[j] = (alpha << 24) | rgb[j];
			}
			cursorImage.setRGB(0, i, cursorImage.getWidth(), 1, rgb, 0, cursorImage.getWidth() * 4);
		}
		Cursor cursor = Toolkit.getDefaultToolkit().createCustomCursor(cursorImage, new Point(0, 0), "CustomCursor");

		frame.setCursor(cursor);
	}

	public static Coordinate getCenterScreen() {
		return new Coordinate(width / 2, height / 2);
	}

	public static int getFrameWidth() {
		return frame.getWidth();
	}

	public static int getFrameHeight() {
		return frame.getHeight();
	}
}
